//
//  Extension.swift
//  UlzzangCamera
//
//  Created by SHOKI TAKEDA on 3/6/16.
//  Copyright © 2016 morningcamera.com. All rights reserved.
//

import UIKit
import CoreImage

//let pixelDataByteSize = 4
//
//extension UIImage {
//    
//    func getColor(pos: CGPoint) -> UIColor {
//        
//        let imageData = CGDataProviderCopyData(CGImageGetDataProvider(self.CGImage))
//        let data : UnsafePointer = CFDataGetBytePtr(imageData)
//        let scale = UIScreen.mainScreen().scale
//        let address : Int = ((Int(self.size.width) * Int(pos.y * scale)) + Int(pos.x * scale)) * pixelDataByteSize
//        let r = CGFloat(data[address])
//        let g = CGFloat(data[address+1])
//        let b = CGFloat(data[address+2])
//        let a = CGFloat(data[address+3])
//        
//        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: a/255)
//    }
//}

extension UIImage{
    
    // UIImageをリサイズするメソッド.
    class func ResizeÜIImage(image : UIImage,width : CGFloat, height : CGFloat)-> UIImage!{
        // 指定された画像の大きさのコンテキストを用意.
        UIGraphicsBeginImageContext(CGSizeMake(width, height))
        // コンテキストに自身に設定された画像を描画する.
        image.drawInRect(CGRectMake(0, 0, width, height))
        // コンテキストからUIImageを作る.
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        // コンテキストを閉じる.
        UIGraphicsEndImageContext()
        return newImage
    }
}

extension UIColor {
    //RGBを取得
    func getRGB() -> (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        var r: CGFloat = 0.0, g: CGFloat = 0.0, b: CGFloat = 0.0, a: CGFloat = 0.0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        return (red: (r * 255.0), green: (g * 255.0), blue: (b * 255.0), alpha: (a * 255.0))
    }
}

extension UIImage {
    
    func getFilteredImage(leftEyeX:Int, leftEyeY:Int, faceMinX:Int, faceMaxX:Int, faceMinY:Int, faceMaxY:Int, imageHeight:CGFloat, imageWidth:CGFloat, viewHeight:CGFloat, viewWidth:CGFloat) -> UIImage {
        var _rtnImage : UIImage!
        
        //色を変換し、base画像に出力する
        _rtnImage = self.getPictureImageFromOriginalColor(leftEyeX, _leftEyeY:leftEyeY, _faceMinX:faceMinX, _faceMaxX:faceMaxX, _faceMinY:faceMinY, _faceMaxY:faceMaxY, _imageHeight:imageHeight, _imageWidth:imageWidth, _viewHeight:viewHeight, _viewWidth:viewWidth )
        
        return _rtnImage
    }
    
    //マスクされた画像を作成
    func getMaskedImage(maskImage:UIImage!) -> UIImage {
        let maskImageReference:CoreImage.CGImage? = maskImage?.CGImage
        let mask = CGImageMaskCreate(CGImageGetWidth(maskImageReference),
            CGImageGetHeight(maskImageReference),
            CGImageGetBitsPerComponent(maskImageReference),
            CGImageGetBitsPerPixel(maskImageReference),
            CGImageGetBytesPerRow(maskImageReference),
            CGImageGetDataProvider(maskImageReference),nil,false)
        let maskedImageReference = CGImageCreateWithMask(self.CGImage, mask)
        let maskedImage = UIImage(CGImage: maskedImageReference!)
        return maskedImage
    }
    
    //入力した色を他の色に変換する
    func getPictureImageFromOriginalColor(_leftEyeX:Int, _leftEyeY:Int, _faceMinX:Int, _faceMaxX:Int, _faceMinY:Int, _faceMaxY:Int, _imageHeight:CGFloat, _imageWidth:CGFloat, _viewHeight:CGFloat, _viewWidth:CGFloat) -> UIImage? {
        
        let _image = self
        let _width = Int(_image.size.width)
        let _height = Int(_image.size.height)
        let _imageData = _image.imageData()
        var imageBytes : UnsafeMutablePointer<UInt8>;
        let newByteLength = _width * _height * 4
        imageBytes = UnsafeMutablePointer<UInt8>.alloc(newByteLength)
        
        let a = Double(_faceMaxY)
        let b = Double(_faceMinY)
        let c = Double(_imageHeight/2)-Double(_viewHeight*_imageWidth/_viewWidth/2)
        let leftCal = Int(Double(_height)-a*Double(_imageWidth/_viewWidth)-c)
        let rightCal = Int(Double(_height)-b*Double(_imageWidth/_viewWidth)-c)
        let calFaceMinX = Double(_faceMinX)*Double(_imageWidth/_viewWidth)
        let calFaceMaxX = Double(_faceMaxX)*Double(_imageWidth/_viewWidth)
        
        let harfFacePointY = Int((leftCal + rightCal)/2)
        let leftEyeX = Int(Double(_leftEyeX)*Double(_imageWidth/_viewWidth))
        let point = (leftEyeX, harfFacePointY)
        let globalColor = UIImage.colorAtPoint(
            point,
            imageWidth: _width,
            withData: _imageData
        )
//        let globalColor = getColor(CGPointMake(CGFloat(leftEyeX), CGFloat(harfFacePointY)))
        var globalSpaceRed:CGFloat = 1.0
        var globalSpaceGreen:CGFloat = 1.0
        var globalSpaceBlue:CGFloat = 1.0
        var globalSpaceAlpha:CGFloat = 1.0
        globalColor.getRed(&globalSpaceRed, green: &globalSpaceGreen, blue: &globalSpaceBlue, alpha: &globalSpaceAlpha)
        
        for x in Int(calFaceMinX)..<Int(calFaceMaxX) {
            for y in leftCal..<rightCal {
                
                let point = (x, y)
                let color = UIImage.colorAtPoint(
                    point,
                    imageWidth: _width,
                    withData: _imageData
                )
//                let color = getColor(CGPointMake(CGFloat(x), CGFloat(y)))
                let i: Int = ((Int(_width) * Int(y)) + Int(x)) * 4
                
                var spaceRed:CGFloat = 1.0
                var spaceGreen:CGFloat = 1.0
                var spaceBlue:CGFloat = 1.0
                var spaceAlpha:CGFloat = 1.0
                color.getRed(&spaceRed, green: &spaceGreen, blue: &spaceBlue, alpha: &spaceAlpha)
                
                if  spaceRed > globalSpaceRed - CGFloat(0.4) && spaceRed < globalSpaceRed + CGFloat(0.4) {
                    imageBytes[i] = UInt8(spaceRed*255) // red
                    if spaceGreen - CGFloat(0.3) < 1 && spaceGreen - CGFloat(0.3) > 0 {
                        imageBytes[i+1] = UInt8((spaceGreen - CGFloat(0.3))*255) // green
                    }
                    if spaceBlue - CGFloat(0.3) < 1 && spaceBlue - CGFloat(0.3) > 0 {
                        imageBytes[i+2] = UInt8((spaceBlue - CGFloat(0.3))*255) // blue
                    }
                    imageBytes[i+3] = 100
                } else {
                }
            }
        }
        let provider = CGDataProviderCreateWithData(nil, imageBytes, newByteLength, nil)
        let bitsPerComponent:UInt = 8
        let bitsPerPixel:UInt = bitsPerComponent * 4
        let bytesPerRow:UInt = UInt(4) * UInt(_width)
        let colorSpaceRef = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.PremultipliedLast.rawValue)
        let renderingIntent = CGColorRenderingIntent.RenderingIntentDefault
        let cgImage = CGImageCreate(_width, _height, Int(bitsPerComponent), Int(bitsPerPixel), Int(bytesPerRow), colorSpaceRef, bitmapInfo, provider, nil, false, renderingIntent)
        return UIImage(CGImage: cgImage!)
    }
    
    //特定した場所のピクセル色を取得する
    class func colorAtPoint(point:(x:Int,y: Int),imageWidth: Int,withData data: UnsafePointer<UInt8>) -> UIColor {
        let offset = 4 * ((imageWidth * point.y) + point.x)
        let r = CGFloat(data[offset])
        let g = CGFloat(data[offset + 1])
        let b = CGFloat(data[offset + 2])
        let a = CGFloat(data[offset + 3])
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: a/255)
    }
    
    func imageData() -> UnsafePointer<UInt8> {
        let pixelData = CGDataProviderCopyData(CGImageGetDataProvider(self.CGImage))
        return CFDataGetBytePtr(pixelData)
    }
    
    func getPixelColor(pos: CGPoint) -> UIColor {
        let pixelData = CGDataProviderCopyData(CGImageGetDataProvider(self.CGImage))
        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)
        let pixelInfo: Int = ((Int(self.size.width) * Int(pos.y)) + Int(pos.x)) * 4
        let r = CGFloat(data[pixelInfo]) / CGFloat(255.0)
        let g = CGFloat(data[pixelInfo+1]) / CGFloat(255.0)
        let b = CGFloat(data[pixelInfo+2]) / CGFloat(255.0)
        let a = CGFloat(data[pixelInfo+3]) / CGFloat(255.0)
        return UIColor(red: r, green: g, blue: b, alpha: a)
    }
}