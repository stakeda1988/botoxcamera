//
//  ViewController.swift
//  UlzzangCamera
//
//  Created by SHOKI TAKEDA on 3/6/16.
//  Copyright © 2016 morningcamera.com. All rights reserved.
//

import UIKit
import CoreImage

class ViewController: UIViewController {
    
    var backImageView : UIImageView!
    var shipImageView : UIImageView!
    var originalImageheight:CGFloat!
    var originalImagewidth:CGFloat!
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let picName:String = "face04.jpg"
        let uiImg:UIImage = UIImage(named:picName)!
        var data = UIImagePNGRepresentation(uiImg)

//        var format = contentTypeForImageData(data!)
//        print(format)
        self.shipImageView = UIImageView(image:UIImage(data:data!))
        self.backImageView = UIImageView(image:UIImage(named:picName))
        originalImageheight = self.shipImageView.frame.height
        originalImagewidth = self.shipImageView.frame.width
        let ratio = self.shipImageView.frame.height / self.shipImageView.frame.width
        self.shipImageView.frame = CGRectMake(0, self.view.frame.height/2 - self.view.frame.width * ratio/2, self.view.frame.width, self.view.frame.width * ratio)
        self.backImageView.frame = CGRectMake(0, self.view.frame.height/2 - self.view.frame.width * ratio/2, self.view.frame.width, self.view.frame.width * ratio)
        self.view.addSubview(self.backImageView)
        self.view.addSubview(self.shipImageView)
        
        let myImage : UIImage = UIImage.ResizeÜIImage(UIImage(named: picName)!, width: self.view.frame.width, height: self.view.frame.height)
        let options : NSDictionary = NSDictionary(object: CIDetectorAccuracyHigh, forKey: CIDetectorAccuracy)
        let detector : CIDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: options as! [String : AnyObject])
        let faces : NSArray = detector.featuresInImage(CIImage(image: myImage)!)
        var transform : CGAffineTransform = CGAffineTransformMakeScale(1, -1)
        transform = CGAffineTransformTranslate(transform, 0, -self.view.frame.height)
        var globalLeftEyeX:Int = 0
        var globalLeftEyeY:Int = 0
        var globalFaceMinX:Int = 0
        var globalFaceMaxX:Int = 0
        var globalFaceMinY:Int = 0
        var globalFaceMaxY:Int = 0
        var faceCounter:Int = 0
        for feature in faces {
            if faceCounter == 0 {
                globalLeftEyeX = Int(feature.leftEyePosition.x)
                globalLeftEyeY = Int(feature.leftEyePosition.y)
                globalFaceMinX = Int(feature.bounds.minX)
                globalFaceMaxX = Int(feature.bounds.maxX)
                globalFaceMinY = Int(feature.bounds.minY)
                globalFaceMaxY = Int(feature.bounds.maxY)
            }
            faceCounter++
        }
        let filteredImage = self.shipImageView?.image!.getFilteredImage(globalLeftEyeX, leftEyeY: globalLeftEyeY, faceMinX: globalFaceMinX, faceMaxX: globalFaceMaxX, faceMinY: globalFaceMinY, faceMaxY: globalFaceMaxY, imageHeight: originalImageheight, imageWidth: originalImagewidth, viewHeight: self.view.frame.height, viewWidth: self.view.frame.width)
        
        let ciContext = CIContext(options: nil)
        let filter = CIFilter(name: "CIPhotoEffectTransfer")
        let ciImage:CIImage = CIImage(image:filteredImage!)!
        filter!.setValue(ciImage, forKey: kCIInputImageKey)
        let filteredImageData = filter!.valueForKey(kCIOutputImageKey) as! CIImage
        let filteredImageRef = ciContext.createCGImage(filteredImageData, fromRect: filteredImageData.extent)
        let newFilteredImage = UIImage(CGImage: filteredImageRef)
        self.shipImageView?.image = newFilteredImage
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//    func contentTypeForImageData(data:NSData) -> String {
//        var c:NSInteger = 0
//        data.getBytes(&c, length:1)
//        var format = ""
//        switch (c) {
//        case 0xFF:
//            format = "image/jpeg"
//        case 0x89:
//            format = "image/png"
//        case 0x47:
//            format = "image/gif"
//        case 0x49: break
//        case 0x4D:
//            format = "image/tiff"
//        default:
//            break
//        }
//        return format
//    }
}

